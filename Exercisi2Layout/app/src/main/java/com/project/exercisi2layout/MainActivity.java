package com.project.exercisi2layout;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText var1;
    TextView var2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        var1 = findViewById(R.id.placeholder);
        var2 = findViewById(R.id.textResult);
    }

    public void sendMessage(View view){
        String n = var1.getText().toString();
        var2.setText(getString(R.string.saludo1)+ " " + n + getString(R.string.saludo2));
    }


    public void deleteMessage(View view) {
        var1.setText("");
        var2.setText("");
    }
}
